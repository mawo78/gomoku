#!/usr/bin/env python

##################################################
# Gomoku bot
##################################################
# Autor: Marcin Wojciechowski
# Email: marcin.wojciechowski@infinit.cl
##################################################

import random
import socket
import json
import sys
from threading import Thread
from common import timing


class GoMoku(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.bot_name = 'KAPITAN_BOMBA'
        self.board = {}
        self.mode = ""
        self.host, self.port, self.ID = "localhost", 6776, "3425"
        self.board_dim = (15, 15)
        self.first = False
        self.stone_type = 1
        self.enemy_stone = 2
        self.step_nr = 0
        self.min_x = 1000
        self.min_y = 1000
        self.max_x = 0
        self.max_y = 0
        self.prev_min_max = {}

        random.seed()
        # Create a socket (SOCK_STREAM means a TCP socket)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Args: ", sys.argv[1:])
        args = sys.argv[1:]
        if len(args) == 2 and "-v" == args[0]:
            # random move mode
            self.mode = args[1]
            if self.mode == "rand":
                self.bot_name = 'RAND_BOMBA'
        else:
            # regular logic
            if len(args) > 0:
                self.host = args[0]
            if len(args) > 1:
                self.port = int(args[1])
            if len(args) > 2:
                self.ID = args[2]

    @staticmethod
    def check_error_msg(err_msg):
        err_obj = json.loads(err_msg)
        if err_obj["message_type"] == 'error':
            body = err_obj["error"]
            if body["message"] == "OK":
                return True
            else:
                print(body["message"], ' ', body["type"])
                return False

    def register(self):
        msg = { 'name' : self.bot_name,
                'id' : self.ID
                }
        msg_str = json.dumps(msg)
        self.sock.send(str.encode(msg_str))

        received = self.sock.recv(1024).decode()
        self.check_error_msg(received)

    def start_game(self, msg):
        body = msg["game"]
        self.board_dim = ( body["board_info"]["cols"], body["board_info"]["rows"])
        self.first = body["player_info"]["first"]
        self.stone_type = body["player_info"]["stone_type"]
        self.board = {}
        self.step_nr = 1
        if self.stone_type == 2:
            self.enemy_stone = 1
        else:
            self.enemy_stone = 2

    def update_board(self, pos, stone):
        if 0 == stone:
            return
        self.board[pos] = stone
        self.min_x = 1000
        self.min_y = 1000
        self.max_x = 0
        self.max_y = 0
        for cor in self.board.keys():
            self.min_x = min(self.min_x, cor[0])
            self.min_y = min(self.min_y, cor[1])
            self.max_x = max(self.max_x, cor[0])
            self.max_y = max(self.max_y, cor[1])

    def save_min_max_state(self):
        self.prev_min_max['min_x'] = self.min_x
        self.prev_min_max['min_y'] = self.min_y
        self.prev_min_max['max_x'] = self.max_x
        self.prev_min_max['max_y'] = self.max_y

    def load_min_max_state(self):
        self.min_x = self.prev_min_max['min_x']
        self.min_y = self.prev_min_max['min_y']
        self.max_x = self.prev_min_max['max_x']
        self.max_y = self.prev_min_max['max_y']

    def ack_move(self, msg):
        indx = msg["opponent_move"]
        y = indx // self.board_dim[0]
        x = indx % self.board_dim[0]
        self.update_board((x, y), self.enemy_stone)
        self.first = True

    def is_move_legal(self, pos):
        x, y = pos
        if x < 0 or y < 0:
            return False
        if x >= self.board_dim[0] or y >= self.board_dim[1]:
            return False
        if self.board.get(pos, 0) != 0:
            return False
        return True

    adj = [(0, 1), (1, 0), (0, -1), (-1, 0), (1, -1), (1, 1), (-1, -1), (-1, 1)]

    def get_adjacent_points(self, pom, c):
        for p in self.adj:
            x = (c[0] + p[0], c[1] + p[1])
            if self.is_move_legal(x):
                pom.add(x)
        return pom

    def count_adjacent_points(self, x):
        pom = set()
        self.get_adjacent_points(pom, x)
        n = 0
        for p in pom:
            if self.board.get(p, 0) != 0:
                n += 1
        return n

    def get_adjacent_legal_moves(self):
        pom = set()
        for c in self.board.keys():
            self.get_adjacent_points(pom, c)
        return pom

    def get_least_adjacent_move(self):
        moves = list(self.get_adjacent_legal_moves())
        random.shuffle(moves)
        lev = 8
        best_move = moves[0]
        for move in moves:
            n_lev = self.count_adjacent_points(move)
            if n_lev < lev:
                lev = n_lev
                best_move = move
        return best_move

    def get_color(self, x, y):
        return self.board.get((x, y), 0)

    def is_threat_a(self, my_color, enemy_color):
        row5, col5, backslash5, slash5 = [], [], [], []
        # sanity check
        if self.board_dim[0] < 5 or self.board_dim[1] < 5:
            return False, row5, col5, backslash5, slash5

        board_get = self.board.get
        minx = max(self.min_x - 1, 0)
        miny = max(self.min_y - 1, 0)
        maxx = min(self.max_x + 2, self.board_dim[0])
        maxy = min(self.max_y + 2, self.board_dim[1])
        if maxx - minx < 5 and maxy - miny < 5:
            return False, row5, col5, backslash5, slash5

        # check in rows
        for y in range(miny, maxy):
            cols = [0, 0, 0]
            stp = 0
            for x in range(minx, maxx):
                if stp >= 5:
                    cols[board_get((x - 5, y), 0)] -= 1
                cols[board_get((x, y), 0)] += 1
                if cols[0] == 1 and cols[enemy_color] == 4:
                    row5.append((x - 4, y))
                stp += 1

        # check in cols
        for x in range(minx, maxx):
            cols = [0, 0, 0]
            stp = 0
            for y in range(miny, maxy):
                if stp >= 5:
                    cols[board_get((x, y - 5), 0)] -= 1
                cols[board_get((x, y), 0)] += 1
                if cols[0] == 1 and cols[enemy_color] == 4:
                    col5.append((x, y - 4))
                stp += 1

        # check in \
        for x in range(minx, maxx - 4):
            for y in range(miny, maxy - 4):
                cols = [0, 0, 0]
                for p in range(5):
                    cols[board_get((x + p, y + p), 0)] += 1
                if cols[0] == 1 and cols[enemy_color] == 4:
                    backslash5.append((x, y))

        # check in /
        for x in range(minx, maxx - 4):
            for y in range(miny, maxy - 4):
                cols = [0, 0, 0]
                for p in range(5):
                    cols[board_get((x + p, y + 4 - p), 0)] += 1
                if cols[0] == 1 and cols[enemy_color] == 4:
                    slash5.append((x, y))

        res = (len(col5) + len(row5) + len(backslash5) + len(slash5)) > 0
        return res, row5, col5, backslash5, slash5

    def check_threat_a(self, threat_a):
        if threat_a[0]:
            if len(threat_a[1]) > 0:
                pos = threat_a[1][0]
                for p in range(5):
                    if self.get_color(pos[0] + p, pos[1]) == 0:
                        return pos[0] + p, pos[1]
            if len(threat_a[2]) > 0:
                pos = threat_a[2][0]
                for p in range(5):
                    if self.get_color(pos[0], pos[1] + p) == 0:
                        return pos[0], pos[1] + p
            if len(threat_a[3]) > 0:
                pos = threat_a[3][0]
                for p in range(5):
                    if self.get_color(pos[0] + p, pos[1] + p) == 0:
                        return pos[0] + p, pos[1] + p
            if len(threat_a[4]) > 0:
                pos = threat_a[4][0]
                for p in range(5):
                    if self.get_color(pos[0] + p, pos[1] + 4 - p) == 0:
                        return pos[0] + p, pos[1] + 4 - p
        return None

    def is_threat_c(self, my_color, enemy_color):
        row5, col5, backslash5, slash5 = [], [], [], []
        # sanity check
        if self.board_dim[0] < 7 or self.board_dim[1] < 7:
            return False, row5, col5, backslash5, slash5

        board_get = self.board.get
        minx = max(self.min_x - 2, 0)
        miny = max(self.min_y - 2, 0)
        maxx = min(self.max_x + 3, self.board_dim[0])
        maxy = min(self.max_y + 3, self.board_dim[1])
        if maxx - minx < 7 and maxy - miny < 7:
            return False, row5, col5, backslash5, slash5

        # check in rows
        for y in range(miny, maxy):
            cols = [0, 0, 0]
            stp = 0
            for x in range(minx, maxx):
                if stp >= 7:
                    cols[board_get((x - 7, y), 0)] -= 1
                cols[board_get((x, y), 0)] += 1
                if cols[0] == 4 and cols[enemy_color] == 3:
                    if board_get((x - 2, y), 0) == enemy_color and board_get((x - 3, y), 0) == enemy_color and board_get((x - 4, y), 0) == enemy_color :
                        row5.append((x - 6, y))
                stp += 1

        # check in cols
        for x in range(minx, maxx):
            cols = [0, 0, 0]
            stp = 0
            for y in range(miny, maxy):
                if stp >= 7:
                    cols[board_get((x, y - 7), 0)] -= 1
                cols[board_get((x, y), 0)] += 1
                if cols[0] == 4 and cols[enemy_color] == 3:
                    if board_get((x, y - 2), 0) == enemy_color and board_get((x, y - 3), 0) == enemy_color and board_get((x, y - 4), 0) == enemy_color:
                        col5.append((x, y - 6))
                stp += 1

        # check in \
        for x in range(minx, maxx - 7 + 1):
            for y in range(miny, maxy - 7 + 1):
                if board_get((x, y), 0) == 0 and board_get((x + 1, y + 1), 0) == 0:
                    if board_get((x + 2, y + 2), 0) == enemy_color and board_get((x + 3, y + 3), 0) == enemy_color and board_get((x + 4, y + 4), 0) == enemy_color:
                        if board_get((x + 5, y + 5), 0) == 0 and board_get((x + 6, y + 6), 0) == 0:
                            backslash5.append((x, y))

        # check in /
        for x in range(minx, maxx - 7 + 1):
            for y in range(miny, maxy - 7 + 1):
                if board_get((x, y + 6), 0) == 0 and board_get((x + 1, y + 5), 0) == 0:
                    if board_get((x + 2, y + 4), 0) == enemy_color:
                        if board_get((x + 3, y + 3), 0) == enemy_color:
                            if board_get((x + 4, y + 2), 0) == enemy_color:
                                if board_get((x + 5, y + 1), 0) == 0 and board_get((x + 6, y), 0) == 0:
                                    slash5.append((x, y))

        res = (len(col5) + len(row5) + len(backslash5) + len(slash5)) > 0
        return res, row5, col5, backslash5, slash5

    def check_threat_c(self, threat_c):
        if threat_c[0]:
            p1, p2 = (0, 0), (0, 0)
            if len(threat_c[1]) > 0:
                x, y = threat_c[1][0][0], threat_c[1][0][1]
                p1 = (x + 1, y)
                p2 = (x + 5, y)
            if len(threat_c[2]) > 0:
                x, y = threat_c[2][0][0], threat_c[2][0][1]
                p1 = (x, y + 1)
                p2 = (x, y + 5)
            if len(threat_c[3]) > 0:
                x, y = threat_c[3][0][0], threat_c[3][0][1]
                p1 = (x + 1, y + 1)
                p2 = (x + 5, y + 5)
            if len(threat_c[4]) > 0:
                x, y = threat_c[4][0][0], threat_c[4][0][1]
                p1 = (x + 1, y + 5)
                p2 = (x + 5, y + 1)

            if self.count_adjacent_points(p1) < self.count_adjacent_points(p2):
                return p1
            else:
                return p2

        return None

    def is_threat_d(self, my_color, enemy_color):
        row5, col5, backslash5, slash5 = [], [], [], []
        # sanity check
        if self.board_dim[0] < 6 or self.board_dim[1] < 6:
            return False, row5, col5, backslash5, slash5

        board_get = self.board.get
        minx = max(self.min_x - 1, 0)
        miny = max(self.min_y - 1, 0)
        maxx = min(self.max_x + 2, self.board_dim[0])
        maxy = min(self.max_y + 2, self.board_dim[1])
        if maxx - minx < 6 and maxy - miny < 6:
            return False, row5, col5, backslash5, slash5

        # check in rows
        for y in range(miny, maxy):
            cols = [0, 0, 0]
            stp = 0
            for x in range(minx, maxx):
                if stp >= 6:
                    cols[board_get((x - 6, y), 0)] -= 1
                cols[board_get((x, y), 0)] += 1
                if cols[0] == 3 and cols[enemy_color] == 3:
                    if board_get((x, y), 0) == 0 and board_get((x - 5, y), 0) == 0:
                        row5.append((x - 5, y))
                stp += 1

        # check in cols
        for x in range(minx, maxx):
            cols = [0, 0, 0]
            stp = 0
            for y in range(miny, maxy):
                cols[board_get((x, y), 0)] += 1
                if stp >= 6:
                    cols[board_get((x, y - 6), 0)] -= 1
                if cols[0] == 3 and cols[enemy_color] == 3:
                    if board_get((x, y), 0) == 0 and board_get((x, y - 5), 0) == 0:
                        col5.append((x, y - 5))
                stp += 1

        # check in \
        for x in range(minx, maxx - 6 + 1):
            for y in range(miny, maxy - 6 + 1):
                cols = [0, 0, 0]
                for p in range(6):
                    cols[board_get((x + p, y + p), 0)] += 1
                if cols[0] == 3 and cols[enemy_color] == 3:
                    if board_get((x, y), 0) == 0 and board_get((x + 5, y + 5), 0) == 0:
                        backslash5.append((x, y))

        # check in /
        for x in range(minx, maxx - 6 + 1):
            for y in range(miny, maxy - 6 + 1):
                cols = [0, 0, 0]
                for p in range(6):
                    cols[board_get((x + p, y + 5 - p), 0)] += 1
                if cols[0] == 3 and cols[enemy_color] == 3:
                    if board_get((x, y + 5), 0) == 0 and board_get((x + 5, y), 0) == 0:
                        slash5.append((x, y))

        res = (len(col5) + len(row5) + len(backslash5) + len(slash5)) > 0
        return res, row5, col5, backslash5, slash5

    def check_threat_d(self, threat_d):
        if threat_d[0]:
            if len(threat_d[1]) > 0:
                x, y = threat_d[1][0]
                for p in range(1, 5):
                    if self.get_color(x + p, y) == 0:
                        return x + p, y
            if len(threat_d[2]) > 0:
                x, y = threat_d[2][0]
                for p in range(1, 5):
                    if self.get_color(x, y + p) == 0:
                        return x, y + p
            if len(threat_d[3]) > 0:
                x, y = threat_d[3][0]
                for p in range(1, 5):
                    if self.get_color(x + p, y + p) == 0:
                        return x + p, y + p
            if len(threat_d[4]) > 0:
                x, y = threat_d[4][0]
                for p in range(1, 5):
                    if self.get_color(x + p, y + 5 - p) == 0:
                        return x + p, y + 5 - p
        return None

    def is_threat_square(self, my_color, enemy_color):
        rowcol, slash = [], []
        # sanity check
        if self.board_dim[0] < 5 or self.board_dim[1] < 5:
            return False, rowcol, slash
        board_get = self.board.get
        minx = max(self.min_x - 1, 0)
        miny = max(self.min_y - 1, 0)
        maxx = min(self.max_x + 2, self.board_dim[0])
        maxy = min(self.max_y + 2, self.board_dim[1])
        if maxx - minx < 5 and maxy - miny < 5:
            return False, rowcol, slash

        for x in range(minx, maxx - 5 + 1):
            for y in range(miny, maxy - 5 + 1):
                if board_get((x, y + 2), 0) == 0 and board_get((x + 1, y + 2), 0) == enemy_color and board_get((x + 2, y + 2), 0) == 0:
                    if board_get((x + 3, y + 2), 0) == enemy_color and board_get((x + 4, y + 2), 0) == 0:
                        if board_get((x + 2, y), 0) == 0 and board_get((x + 2, y + 1), 0) == enemy_color:
                            if board_get((x + 2, y + 3), 0) == enemy_color and board_get((x + 2, y + 4), 0) == 0:
                                rowcol.append((x, y))

        for x in range(minx, maxx - 5 + 1):
            for y in range(miny, maxy - 5 + 1):
                if board_get((x, y), 0) == 0 and board_get((x + 1, y + 1), 0) == enemy_color and board_get((x + 2, y + 2), 0) == 0:
                    if board_get((x + 3, y + 3), 0) == enemy_color and board_get((x + 4, y + 4), 0) == 0:
                        if board_get((x, y + 4), 0) == 0 and board_get((x + 1, y + 3), 0) == enemy_color:
                            if board_get((x + 3, y + 1), 0) == enemy_color and board_get((x + 4, y), 0) == 0:
                                slash.append((x, y))

        res = (len(rowcol) + len(slash)) > 0
        return res, rowcol, slash

    @staticmethod
    def check_threat_square(threat_sqr):
        if threat_sqr[0]:
            if len(threat_sqr[1]) > 0:
                x, y = threat_sqr[1][0]
                return x + 2, y + 2
            if len(threat_sqr[2]) > 0:
                x, y = threat_sqr[2][0]
                return x + 2, y + 2
        return None

    @staticmethod
    def calculate_part_threat(thr_a):
        threat = 0
        if thr_a[0]:
            n = 0
            for thr in thr_a[1:]:
                n += len(thr)
            threat += n
        return threat

    def calculate_threat(self, my_color):
        if 1 == my_color:
            enemy_color = 2
        else:
            enemy_color = 1

        threat = 0
        thr_a = self.is_threat_a(enemy_color, my_color)
        threat += self.calculate_part_threat(thr_a) * 1000
        thr_c = self.is_threat_c(enemy_color, my_color)
        threat += self.calculate_part_threat(thr_c) * 100
        thr_d = self.is_threat_d(enemy_color, my_color)
        threat += self.calculate_part_threat(thr_d) * 10
        thr_sqr = self.is_threat_square(enemy_color, my_color)
        threat += self.calculate_part_threat(thr_sqr) * 200
        return threat

    def find_max_threating_move(self, moves, my_color):
        best_score = 0
        best_move = moves[0]
        for move in moves:
            self.save_min_max_state()
            self.update_board(move, my_color)
            score = self.calculate_threat(my_color)
            if score > best_score:
                best_move = move
                best_score = score
            del self.board[move]
            self.load_min_max_state()
        if best_score == 0:
            # best_move = moves[random.randint(0, len(moves) - 1)]
            best_move = self.get_least_adjacent_move()
        print(best_score, "\t", best_move)
        return best_move, best_score

    @timing
    def get_next_pos(self):
        win_a = self.is_threat_a(self.enemy_stone, self.stone_type)
        wa = self.check_threat_a(win_a)
        if wa:
            print("Win A ", wa)
            return wa

        threat_a = self.is_threat_a(self.stone_type, self.enemy_stone)
        ta = self.check_threat_a(threat_a)
        if ta:
            print("Threat A ", ta)
            return ta

        threat_c = self.is_threat_c(self.stone_type, self.enemy_stone)
        ta = self.check_threat_c(threat_c)
        if ta:
            print("Threat c ", ta)
            return ta

        threat_d = self.is_threat_d(self.stone_type, self.enemy_stone)
        ta = self.check_threat_d(threat_d)
        if ta:
            print("Threat d ", ta)
            return ta

        threat_sqr = self.is_threat_square(self.stone_type, self.enemy_stone)
        ta = self.check_threat_square(threat_sqr)
        if ta:
            print("Threat sqr ", ta)
            return ta

        win_c = self.is_threat_c(self.enemy_stone, self.stone_type)
        wa = self.check_threat_c(win_c)
        if wa:
            print("Win c ", wa)
            return wa

        win_d = self.is_threat_d(self.enemy_stone, self.stone_type)
        wa = self.check_threat_d(win_d)
        if wa:
            print("Win d ", wa)
            return wa

        win_sqr = self.is_threat_square(self.enemy_stone, self.stone_type)
        wa = self.check_threat_square(win_sqr)
        if wa:
            print("Win sqr ", wa)
            return wa

        pos_set = self.get_adjacent_legal_moves()
        moves = list(pos_set)
        if 0 == len(moves):
            print("start")
            return self.board_dim[0] // 2, self.board_dim[1] // 2
        else:
            if "rand" == self.mode:
                return moves[random.randint(0, len(pos_set) - 1)]
            else:
                print("max threat 1")
                my_threat_move, threat = self.find_max_threating_move(moves, self.stone_type)
                enemy_threat_move, en_threat = self.find_max_threating_move(moves, self.enemy_stone)
                if 0 == threat:
                    if 0 == en_threat:
                        return self.get_least_adjacent_move() #my_threat_move
                    return enemy_threat_move
                if threat < en_threat:
                    return enemy_threat_move
                return my_threat_move

        # x = random.randint(0, self.board_dim[0]-1)
        # y = random.randint(0, self.board_dim[1]-1)
        # if not (x, y) in self.board.keys():
        #     return x, y
        # else:
        #     return self.get_next_pos()

    def make_move(self):
        pos = self.get_next_pos()
        p = pos[0] + pos[1] * self.board_dim[0]
        msg = { "index" : p }
        msg_str = json.dumps(msg)
        print("step ", self.step_nr, msg_str)
        self.step_nr += 1
        self.sock.send(str.encode(msg_str))
        self.first = False
        self.update_board(pos, self.stone_type)

    def run(self):
        try:
            # Connect to server and send data
            #self.sock.settimeout(10)
            print("Connecting to ", self.host, self.port)
            self.sock.connect((self.host, self.port))
            while 1:
                received = self.sock.recv(1024).decode()
                if len(received) == 0 or received == '{}':
                    continue  # ???
                a = json.loads(received)
                # print(json.dumps(a))
                if a["message_type"] == 'register':
                    self.register()
                elif a["message_type"] == 'game_start':
                    self.start_game(a)
                elif a["message_type"] == 'game_move':
                    self.ack_move(a)
                elif a["message_type"] == 'game_over':
                    print("Game over! ", a["winner"])
                    #break
                elif a["message_type"] == 'error':
                    if a["error"]["type"] == 12:
                        print("waiting for new game...")
                        #time.sleep(0.5)
                    elif a["error"]["type"] == 0:
                        print(".")
                    else:
                        print(a["error"]["message"])
                        break

                if self.first:
                    self.make_move()
                #time.sleep(0.1)
                # sock.sendall(data)
                #while 1:
                #    print('Client sent:', self.sock.recv(1024).decode())
                #    self.sock.send(b'Oi you sent something to me')
        finally:
            self.sock.close()


if __name__ == '__main__':
    cl = GoMoku()
    cl.start()
