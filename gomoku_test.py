import io
import pstats
import random
import unittest
from pstats import SortKey

from main import GoMoku
from common import timing
import cProfile


class MyTestCase(unittest.TestCase):
    def MyTestCase(self):
        self.gomoku = GoMoku()

    def fill_with_rand(self, gomoku):
        random.seed(42)
        for x in range(gomoku.board_dim[0]//2):
            for y in range(gomoku.board_dim[1]//2):
                gomoku.update_board((x*2, y*2), random.randint(0, 2))

    def test_threat_a_slash(self):
        gomoku = GoMoku()
        gomoku.update_board((10,14), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((11,13), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((12,12), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((13,11), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(True, a[0])
        self.assertEqual((10,10), a[4][0])

    def test_threat_a_backslash(self):
        gomoku = GoMoku()
        gomoku.update_board((11,11), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((12,12), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((13,13), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((14,14), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(True, a[0])
        self.assertEqual((10,10), a[3][0])

    def test_threat_a_row(self):
        gomoku = GoMoku()
        gomoku.update_board((10,10), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((13,10), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((12,10), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(False, a[0])

        gomoku.update_board((11,10), 1)
        a = gomoku.is_threat_a(2, 1)
        self.assertEqual(True, a[0])

    def test_threat_c(self):
        gomoku = GoMoku()
        gomoku.update_board((10,10), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((11,9), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((12,8), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(True, a[0])
        a = gomoku.is_threat_d(2, 1)
        self.assertEqual(False, a[0])

    def test_threat_c_slash(self):
        gomoku = GoMoku()
        gomoku.update_board((11,11), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((12,10), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((10,12), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(True, a[0])

    def test_threat_c_slash_f(self):
        gomoku = GoMoku()
        gomoku.update_board((12,11), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((13,10), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((11,12), 1)
        a = gomoku.is_threat_c(2, 1)
        self.assertEqual(False, a[0])

    def test_is_threat_d(self):
        gomoku = GoMoku()
        # /
        gomoku.update_board((10,10), 1)
        gomoku.update_board((11,9), 1)
        a = gomoku.is_threat_d(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((13,7), 1)
        a = gomoku.is_threat_d(2, 1)
        self.assertEqual(True, a[0])

        gomoku = GoMoku()
        # row
        gomoku.update_board((10,10), 1)
        gomoku.update_board((11,10), 1)
        a = gomoku.is_threat_d(2, 1)
        self.assertEqual(False, a[0])
        gomoku.update_board((13,10), 1)
        a = gomoku.is_threat_d(2, 1)
        self.assertEqual(True, a[0])

    @timing
    def test_timing_19(self):
        pr = cProfile.Profile()
        pr.enable()
        gomoku = GoMoku()
        gomoku.board_dim = (20, 20)
        self.fill_with_rand(gomoku)
        gomoku.update_board((10,10), 1)
        gomoku.update_board((11,9), 1)
        gomoku.update_board((13,7), 1)
        #gomoku.get_next_pos()

        pos_set = gomoku.get_adjacent_legal_moves()
        moves = list(pos_set)
        my_threat_move, threat = gomoku.find_max_threating_move(moves, gomoku.stone_type)
        #enemy_threat_move, en_threat = gomoku.find_max_threating_move(moves, gomoku.enemy_stone)
        pr.disable()
        s = io.StringIO()
        sortby = SortKey.CUMULATIVE
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())

    @timing
    def test_timing1(self):
        self.gomoku = GoMoku()
        get_color = self.gomoku.get_color
        board = self.gomoku.board
        a=0
        for i in range(10000):
            for x in range(20):
                for y in range(20):
                    if board.get((x, y)):
                        a += 1

    @timing
    def test_timing2(self):
        self.gomoku = GoMoku()
        tab = [[0 for x in range(15)] for y in range(15)]
        a=0
        for i in range(10000):
            for x in range(15):
                for y in range(15):
                    a += tab[x][y]

    # def test_time(self):
    #     self.timing_test1()
    #     self.timing_test2()


if __name__ == '__main__':
    unittest.main()
